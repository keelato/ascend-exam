package com.ascendcorp.exam.proxy;

import com.ascendcorp.exam.model.InquiryRequest;
import com.ascendcorp.exam.model.TransferResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;


public class BankProxyGateway {

    public TransferResponse requestTransfer(InquiryRequest inquiryRequest) {

        return new TransferResponse();
    }
}

